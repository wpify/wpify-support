<?php
/*
 * Plugin Name:       WPify Support
 * Description:       By activating this plugin, you will provide access to WPify support with email support@wpify.io. To remove the access, deactivate the plugin.
 * Version:           WPIFY_VERSION
 * Requires PHP:      7.4
 * Requires at least: 4.9
 * Tested up to:      6.5
 * Author:            WPify
 * Author URI:        https://wpify.io/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       wpify-support
 * Domain Path:       /languages
 * Network:           true
*/

const WPIFY_SUPPORT_USER         = 'wpify';
const WPIFY_SUPPORT_EMAIL        = 'support@wpify.io';
const WPIFY_SUPPORT_API_ADD      = 'https://wpify.io/wp-json/wpify-support-access/v1/add';
const WPIFY_SUPPORT_API_REMOVE   = 'https://wpify.io/wp-json/wpify-support-access/v1/remove';

/**
 * This function registers the update checker. If we find a way how to improve the support plugin or find a bug
 * in it, it's always helpful to be able to distribute the new version of Support plugin to those who needs that.
 */
function wpify_support_register_update_checker() {
	require_once __DIR__ . '/vendor/yahnis-elsts/plugin-update-checker/plugin-update-checker.php';

	Puc_v4_Factory::buildUpdateChecker(
			'https://wpify.io/downloads/wpify-support.json',
			__FILE__,
			'wpify-support'
	);
}

/**
 * This finds a user with email support@wpify.io. If the user exists, it returns that user.
 *
 * @param null $network_wide
 *
 * @return false|mixed|WP_User|null
 */
function wpify_support_find_user( $network_wide = null ) {
	require_once ABSPATH . '/wp-admin/includes/user.php';

	if ( is_null( $network_wide ) ) {
		$network_wide = is_plugin_active_for_network( plugin_basename( __FILE__ ) );
	}

	if ( $network_wide ) {
		$users = get_users(
				array(
						'blog_id'        => 0,
						'search'         => WPIFY_SUPPORT_EMAIL,
						'search_columns' => array( 'user_email' )
				)
		);

		$user = ! empty( $users[0] ) ? $users[0] : null;
	} else {
		$user = get_user_by( 'email', WPIFY_SUPPORT_EMAIL );
	}

	return $user;
}

/**
 * When the plugin is activated, we need to create a user with the email support@wpify.io and give it the
 * administrator privileges. It works both for multisite and normal sites.
 *
 * @param $network_wide
 *
 * @return false|mixed|WP_User|null
 */
function wpify_support_create_administrator( $network_wide ) {
	$user = wpify_support_find_user( $network_wide );

	if ( empty( $user ) ) {
		$pass = wp_generate_password( 20 );

		if ( $network_wide ) {
			$user_id = wpmu_create_user( WPIFY_SUPPORT_USER, $pass, WPIFY_SUPPORT_EMAIL );
			grant_super_admin( $user_id );
			$user = new WP_User( $user_id );
		} else {
			$user_id = wp_create_user( WPIFY_SUPPORT_USER, $pass, WPIFY_SUPPORT_EMAIL );
			$user    = new WP_User( $user_id );
			$user->set_role( 'administrator' );
		}

		$user->display_name = __( 'WPify Support', 'wpify-support' );
		$user->first_name   = __( 'Support', 'wpify-support' );
		$user->last_name    = __( 'WPify', 'wpify-support' );
		$user->nickname     = __( 'WPify Support', 'wpify-support' );
		$user->user_url     = __( 'https://wpify.io', 'wpify-support' );
		$user->description  = __( 'This is a profile of WPify Support user that has a full access to your site. When you no longer need the support, you can remove the user by deactivating WPify Support plugin on your site.', 'wpify-support' );

		wp_update_user( $user );
		delete_option( 'wpify_support_admin_notice_dismissed' );
		wp_remote_post( WPIFY_SUPPORT_API_ADD, array(
				'method'   => 'POST',
				'blocking' => false,
				'body'     => array(
						'username'  => WPIFY_SUPPORT_USER,
						'password'  => $pass,
						'login_url' => wp_login_url(),
				)
		) );
	}

	return $user;
}

/**
 * When the plugin is being deactivated, we remove the user with the email support@wpify.io. That is the point
 * when you don't need our support.
 *
 * @param $network_wide
 */
function wpify_support_remove_administrator( $network_wide ) {
	$user = wpify_support_find_user( $network_wide );

	if ( $user ) {
		if ( $network_wide ) {
			require_once ABSPATH . '/wp-admin/includes/ms.php';

			revoke_super_admin( $user->ID );
			wpmu_delete_user( $user->ID );
		} else {
			wp_delete_user( $user->ID, 1 );
		}

		wp_remote_post( WPIFY_SUPPORT_API_REMOVE, array(
				'method'   => 'POST',
				'blocking' => false,
				'body'     => array(
						'username'  => WPIFY_SUPPORT_USER,
						'login_url' => wp_login_url(),
				)
		) );

		delete_option( 'wpify_support_admin_notice_dismissed' );
	}
}

/**
 * When we have an access to your site, we want to inform you about that, so you are aware that WPify Support
 * has the access and how to remove it.
 */
function wpify_support_display_notice() {
	$user = wpify_support_create_administrator( is_plugin_active_for_network( plugin_basename( __FILE__ ) ) );

	if ( get_option( 'wpify_support_admin_notice_dismissed' ) || ! current_user_can( 'administrator' ) ) {
		return;
	}

	if ( $user ) {
		?>
		<div class="notice notice-info is-dismissible wpify-support-notice">
			<p><?php
				/** @noinspection HtmlUnknownTarget */
				echo sprintf(
						__( 'The WPify Support <a href="%1$s">has access</a> to your site. When you no longer need our help, please remove the access by <a href="%2$s">deactivating</a> the WPify Support plugin.', 'wpify-support' ),
						get_edit_user_link( $user->ID ),
						add_query_arg( array(
								'action'   => 'deactivate',
								'plugin'   => urlencode( plugin_basename( __FILE__ ) ),
								'_wpnonce' => wp_create_nonce( 'deactivate-plugin_' . plugin_basename( __FILE__ ) )
						), admin_url( 'plugins.php' ) )
				);
				?></p>
		</div>
		<?php
	}
}

/**
 * Of course,if you don't want to be bothered by the message that we have an access to your site, you can dismiss
 * that message.
 */
function wpify_support_make_admin_notice_dismissable() {
	if ( get_option( 'wpify_support_admin_notice_dismissed' ) || ! current_user_can( 'administrator' ) ) {
		return;
	}

	$script = "jQuery(document).on('click','.wpify-support-notice .notice-dismiss',function(){jQuery.post(ajaxurl,{action:'wpify_support_dismiss_notice'});});";
	wp_add_inline_script( 'jquery', $script );
}

/**
 * This function saves the information that you dismissed the message that we have an access to your site.
 */
function wpify_support_dismiss_admin_notice() {
	update_option( 'wpify_support_admin_notice_dismissed', true );
}

/**
 * We want you to provide you a message in your language, so we load a localization of the plugin.
 */
function wpify_support_load_textdomain() {
	load_plugin_textdomain( 'wpify-support', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

// When the plugin is activated, create an administrator on you site
register_activation_hook( __FILE__, 'wpify_support_create_administrator' );

// When the plugin is deactivated, remove the administrator from your site
register_deactivation_hook( __FILE__, 'wpify_support_remove_administrator' );

// Display the message that we have an access and make it dismissible
add_action( 'admin_notices', 'wpify_support_display_notice' );
add_action( 'network_admin_notices', 'wpify_support_display_notice' );
add_action( 'admin_enqueue_scripts', 'wpify_support_make_admin_notice_dismissable' );
add_action( 'wp_ajax_wpify_support_dismiss_notice', 'wpify_support_dismiss_admin_notice' );

// Load the update checker for the plugin
add_action( 'plugins_loaded', 'wpify_support_register_update_checker' );

// Handle the localization of the plugin, so you can see that in your language
add_action( 'init', 'wpify_support_load_textdomain' );
