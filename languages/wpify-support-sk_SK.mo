��    	      d      �       �   �   �      o  �   w  �   -     �     �     �       �  !  �   �     M  �   U  �        �     �     �     �                                      	        By activating this plugin, you will provide access to WPify support with email support@wpify.io. To remove the access, deactivate the plugin. Support The WPify Support <a href="%1$s">has access</a> to your site. When you no longer need our help, please remove the access by <a href="%2$s">deactivating</a> the WPify Support plugin. This is a profile of WPify Support user that has a full access to your site. When you no longer need the support, you can remove the user by deactivating WPify Support plugin on your site. WPify WPify Support https://wpify.io https://wpify.io/ Project-Id-Version: WPify Support WPIFY_VERSION
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wpify-support
PO-Revision-Date: 2021-08-05 23:53+0200
Last-Translator: 
Language-Team: 
Language: sk_SK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
X-Domain: wpify-support
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n>=2 && n<=4 ? 1 : 2);
 Aktiváciou tohto pluginu poskytnete prístup podpore WPify s e-mailom support@wpify.io. Ak chcete prístup zrušiť, deaktivujte plugin. Podpora Podpora WPify <a href="%1$s">má prístup</a> k vášmu webu. Keď už našu pomoc nepotrebujete, odstráňte prístup <a href="%2$s">deaktiváciou</a> pluginu Podpora WPify. Toto je profil používateľa podpory WPify, ktorý má plný prístup k vašej stránke. Keď už podporu nepotrebujete, môžete používateľa odstrániť deaktiváciou pluginu Podpora WPify na vašom webe. WPify Podpora WPify https://wpify.io/cs/ https://wpify.io/cs/ 