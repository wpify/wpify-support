=== Wpify Support ===
Contributors: wpify
Tags: WPify, WordPress, WooCommerce
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

By activating this plugin, you will provide access to WPify support with email support@wpify.io. To remove the access, deactivate the plugin.

== Description ==

By activating this plugin, you will provide access to WPify support with email support@wpify.io. To remove the access, deactivate the plugin.

Get support on https://wpify.io

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the plugin files to the `/wp-content/plugins/wpify-support` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 1.0.1 - 1.0.6 =
* Bugfixes

= 1.0.0 =
* Initial version
